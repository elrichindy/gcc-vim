set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.

Plugin 'Valloric/YouCompleteMe'
" autogenerate completer file
Plugin 'rdnetto/YCM-Generator'
" handle git
Plugin 'tpope/vim-fugitive'
" mulitple cursors
Plugin 'terryma/vim-multiple-cursors'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" just in case of using a different shell such as fish make sure to set the
" vim shell to bash
set shell=/bin/bash

noremap <f12> :call ToggleMouse() <CR>
function! ToggleMouse()
  if &mouse == 'a'
    set mouse=
    "set nonumber
    echo "Mouse usage disabled"
  else
    set mouse=a
    echo "Mouse usage enabled"
    endif
endfunction

set backspace=indent,eol,start
set tabstop=4
set expandtab
set autoindent
set number
set scrollbind
set mouse=r
set encoding=utf-8

if &term =~ '^screen'
    " tmux knows the extended mouse mode
set ttymouse=xterm2
endif

set shiftwidth=4
set softtabstop=4
set visualbell
set confirm
set ignorecase
set smartcase
set t_Co=256
set hlsearch
set history=200
syntax on
colorscheme luna-term
